import openpnm.models as mods
from openpnm.geometry import GenericGeometry

class Expansion(GenericGeometry):#, pdim=None, tdim=None, tarea=None):
    r"""

    Geometry of Expansion network

    """

    def __init__(self, pdim=None, tdim=None, tarea=None,**kwargs):
        super().__init__(**kwargs)

        self['pore.diameter'] = pdim
        self['throat.diameter'] = tdim
        self['throat.area'] = tarea

        self.add_model(propname='throat.centroid',
                       model=mods.geometry.throat_centroid.pore_coords)

        self.add_model(propname='pore.area',
                       model=mods.geometry.pore_cross_sectional_area.sphere,
                       pore_diameter='pore.diameter')

        self.add_model(propname='pore.volume',
                       model=mods.geometry.pore_volume.sphere,
                       pore_diameter='pore.diameter')

        self.add_model(propname='throat.endpoints',
                       model=mods.geometry.throat_endpoints.spherical_pores,
                       pore_diameter='pore.diameter',
                       throat_diameter='throat.diameter')

        self.add_model(propname='throat.length',
                       model=mods.geometry.throat_length.piecewise,
                       throat_endpoints='throat.endpoints')

        self.add_model(propname='throat.surface_area',
                       model=mods.geometry.throat_surface_area.cylinder,
                       throat_diameter='throat.diameter',
                       throat_length='throat.length')

        self.add_model(propname='throat.volume',
                       model=mods.geometry.throat_volume.cylinder,
                       throat_diameter='throat.diameter',
                       throat_length='throat.length')

        self.add_model(propname='throat.conduit_lengths',
                       model=mods.geometry.throat_length.conduit_lengths,
                       throat_endpoints='throat.endpoints',
                       throat_length='throat.length')
