# Sub-volume analysis of pore-network simulations

This repository contains the codes to apply the sub-volume analysis and to determine the longitudinal dispersion coefficient from pore-network simulations, proposed in the following publication:

*Alex V.L. Machado, Paulo L.C. Lage, Paulo Couto, Sub-volume analysis of pore-network simulations: Determining the asymptotic longitudinal dispersion coefficient, Advances in Water Resources, Volume 181, 2023, 104541, ISSN 0309-1708, https://doi.org/10.1016/j.advwatres.2023.104541.*

## OpenPNM v2.6.0 modified

We developed the SVA using OpenPNM v2.6.0 with some modifications to have more control over tracer transport simulations in large networks.

Therefore, to run all the codes presented here, you must install OpenPNM from the codes provided in the *OpenPNM_260* directory, preferably in Python version 3.9, using conda/miniconda in a Linux environment. Newer versions of Python may have installation conflicts.

Once the miniconda/conda environment with Python 3.9 was created, inside the *OpenPNM_260* directory, run the following codes on the terminal. Ps: Pardiso is used to solve linear systems.

```
conda install pip
pip install -e .
pip install pypardiso==0.3.1
pip install tqdm
```

## Tutorial to run the SVA application

The *sva* directory contains all the codes to simulate tracer transport in the pore network and apply the SVA to obtain the longitudinal dispersion coefficient evolution.

The test case was configured for the C1 network for repository space constraints. However, other statistically built pore networks can be created using the codes presented in the *networkConstruction* directory.

After installing the modified OpenPNM v2.6.0, run the SVA application for the C1 network:
```
./Allrun
```
The simulation parameters, such as Péclet Number, are introduced in the *simulationParameters.py* file and can be modified.

The transport simulation on an i7 computer with 16 GB of RAM took approximately 2 hours.

## pore network construction

The *networkConstruction* directory contains the codes to create cubic pore networks and pore networks with variable connectivity. More details of this algorithm are in Appendix B of the cited publication.
