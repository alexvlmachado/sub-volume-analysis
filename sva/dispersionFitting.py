import openpnm as op
import numpy as np
import matplotlib.pyplot as plt
#from scipy.optimize import curve_fit
from sampling import *
import scipy.odr as odr
import scipy.stats as stats
import time
import sys
plt.rcParams.update({'font.size': 14})
from networkProperties import networkProperties
from simulationParameters import *

testName = netName.split('.')[0]+'_ad_sim.pnm'
ws = op.Workspace()
#ws.settings["loglevel"] = 50

timeZero = time.time()
proj = op.io.PNM.load_project(testName)
print('Elapsed time reading ',testName ,'= ', (time.time()-timeZero)/60, ' min')

pn = proj.network
geo = proj.geometries('geo_01')
water = proj.phases('phase_01')
phys = proj.physics('phys_01')
sf = proj.algorithms('alg_01')
ad = proj.algorithms('alg_02')
print('Np, Nt', pn.Np, pn.Nt)
water.update(ad.results())
#print(ad.settings)
#pn.project.export_data(phases=water, filename=testName+'dispersion_pv', filetype='xdmf')


inlet=pn.pores('inlet')
outlet=pn.pores('outlet')
internal=pn.pores('internal')
# eliminate dead end pores

print('\nNumber of pores =', pn.Np)
print('\nNumber of throats =', pn.Nt)
Lx = pn['pore.coords'][:, 0].max() - pn['pore.coords'][:, 0].min()
Ly = pn['pore.coords'][:, 1].max() - pn['pore.coords'][:, 1].min()
Lz = pn['pore.coords'][:, 2].max() - pn['pore.coords'][:, 2].min()
print('\nLx Ly Lz', Lx, Ly, Lz)
#sys.exit()

prop = networkProperties(pn)
Lstar = prop.get_Lstar()
spacing = prop.sp
Lcol = prop.Lx
perm = prop.get_permeability()
porosity = prop.get_porosity()
print('Lcol', Lcol)
print('lstar', Lstar)

dp = water['pore.pressure'][inlet]

NpX = int(Lcol/spacing)
print('Number of pores in X', NpX)
Vp = geo['pore.effective_volume'][internal].sum()

Dm = water['pore.diffusivity'].mean()
print('Dm', Dm)
print(sf.rate(pores=inlet, mode='single'))
print(sf.rate(pores=outlet, mode='single'))

flowRate = np.squeeze(sf.rate(pores=inlet))
vel = flowRate*Lcol/Vp
print('intrinsic velocity inlet m/s', vel)

#mu = water['pore.viscosity'].mean()
#print(perm/mu*dp/Lcol/porosity)
#vel = perm/mu*dp/Lcol/porosity
pe = vel*Lstar/Dm
#sys.exit()
#times_str = np.loadtxt(testName+'t_solns.txt', dtype='str')
IDmin = int(0.11*Lcol/vel/ad.settings['t_output'])
IDmax = int(0.85*Lcol/vel/ad.settings['t_output'])
timeStepIDs = np.linspace(IDmin, IDmax,30, dtype=int)

times_str = ad.settings['t_solns']
#t_sp = np.array(times_str)[IDmin:IDmax]
t_sp = np.array(times_str)[timeStepIDs]
print('len t_sp', len(t_sp))

subvol1, subvol2 = set_subvolumes(pn, NpX, spacing, sv1, sv2)

#print(pn['pore.effective_volume'][subvol1[10]].std()/pn['pore.effective_volume'][subvol1[10]].mean())
#sys.exit()
##########################################################
#######     Longitudinal Dipersion Fitting    ############
##########################################################
print('\nLongitudinal dispersion fitting')

t_all = []
xc_all = []
ca_all = []
xc_dev_all = []
ca_dev_all = []

def c_an_A4(dl, xi):
    return c_an_A4_(xi, tc, dl, 1.0, vel, Lcol, 1e-8)

timeZero = time.time()

t_s = []
DLs = []
DLs_sd = []
t_d = [] 

for count, ti in enumerate(t_sp):
    print('\nfitting time = ', ti, '/',np.float_(ti)*vel/Lcol)
    xc, xc_dev, ca, ca_sd = new_sampling(pn, water['pore.concentration@'+str(ti)], subvol1, subvol2)
    t = stats.t.interval(0.95,len(ca)-1)
    tc =np.full(len(xc), np.float_(ti))
    ade_model = odr.Model(c_an_A4)
    ld_data = odr.RealData(xc, ca, sx=xc_dev, sy=ca_sd)
    p0 = [8*Dm]
    ld_odr = odr.ODR(ld_data, ade_model, beta0=p0, maxit=2000, partol=1e-20, sstol=1e-25)
    ld_out = ld_odr.run()
    DL = np.squeeze(ld_out.beta)
    DL_sd = np.squeeze(ld_out.sd_beta)
    print('Longitudinal dispersion = ', DL/Dm, ', error (95% confidence) = ', DL_sd*t[1]/Dm)
    print('Pe column = ', vel*Lcol/DL)
    ca_an = c_an_A4(DL, xc)
    print('Elapsed time during fitting time ti', (time.time()-timeZero)/60)
    DLs.append(DL/Dm)
    DLs_sd.append(DL_sd/Dm*t[1])
    t_s.append(np.float_(ti))
    t_d.append(np.float_(ti)*vel/Lcol)
    data = np.column_stack([xc, xc_dev, ca, ca_sd, ca_an])
    datafile_path = 'profiles@'+str(np.float_(ti)*vel/Lcol)+'.dat'
    header = 'xc, xc_dev, ca, ca_sd, ca_an'
    np.savetxt(datafile_path , data, delimiter=',', header=header)


t_s = np.array(t_s)
t_d = np.array(t_d)
DLs = np.array(DLs)
DLs_sd = np.array(DLs_sd)

print('Saving Dv data')
data = np.column_stack([t_s,t_d, DLs, DLs_sd])
datafile_path = 'Dvs.dat'
header = 'time (s),dimensionless time, DL/Dm, DL/Dm error'
np.savetxt(datafile_path , data, delimiter=',', header=header)
