import numpy as np
import matplotlib.pyplot as plt

t_s, t_d, Dvs, eDvs = np.loadtxt('Dvs.dat', delimiter=',', unpack=True)

fig, ax = plt.subplots()
ax.grid()
ax.errorbar(t_s/3600, Dvs,  marker='x', linestyle='',yerr=eDvs)
plt.xlabel('Time (h)')
plt.ylabel('$\mathcal{D}_v$')

fig, ax = plt.subplots()
ax.grid()
ax.errorbar(t_d, Dvs,  marker='x', linestyle='',yerr=eDvs)
plt.xlabel('$T$')
plt.ylabel('$\mathcal{D}_v$')
plt.show()
