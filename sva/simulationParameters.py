# Input parameters of advection-diffusion simulation and the sub-volume analysis application

#Network name
netName = 'C1.pnm'
# Microscopic Péclet number
peclet = 1.0
# Size of the main sub-volumes
sv1 = 9 
# Size of the auxiliary sub-volumes
sv2 = 11
