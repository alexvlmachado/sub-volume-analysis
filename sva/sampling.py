#import openpnm as op
import numpy as np
import openpnm as op
from scipy.special import erfc

def set_subvolumes(net, ncells, sp, lsv1, lsv2):
    sv1 = int((lsv1-1)/2)
    sv2 = int((lsv2-1)/2)
    coords = net.coords[:,0][net.pores('internal')]
    cmin = coords.min()
    cmax = coords.max()
    revf,step = np.linspace(start=cmin, stop=cmax, num=ncells+1, retstep=True)
    size = ncells-sv2*2
    subvol1 = []
    subvol2 = []
    NpSub = 0
    NpSub2 = 0
    j = 0
    for i in range(size):
        sub = np.where((coords > revf[i+sv2]-sp*sv1) & (coords < revf[i+sv2+1]+sp*sv1))
        sub2 = np.where((coords > revf[i+sv2]-sp*sv2) & (coords < revf[i+sv2+1]+sp*sv2))
        if j > 3:
            subvol1.append(sub)
            subvol2.append(sub2)
            j = 0
        NpSub += len(np.squeeze(sub))
        NpSub2 += len(np.squeeze(sub2))
        j += 1 
    print('No pores vols 1 e 2', NpSub/(size), NpSub2/(size))
    return subvol1, subvol2

def new_sampling(net, ca, subvol1, subvol2):
    coords = net.coords[:,0][net.pores('internal')]
    size = len(subvol1)
    center = np.zeros(size)
    center_dev = np.zeros(size)
    ca_avg = np.zeros(size)
    ca_sd = np.zeros(size)
    NpSub = 0
    NpSub2 = 0
    for i in range(size):
        sub = subvol1[i]
        sub2 = subvol2[i]
        npSv1 = len(np.squeeze(sub))
        npSv2 = len(np.squeeze(sub2))
        NpSub += npSv1
        NpSub2 += npSv2
        vol = net['pore.effective_volume'][sub]
        vol2 = net['pore.effective_volume'][sub2]
        Rp1 = vol/vol.mean()
        center[i] = np.sum(coords[sub]*vol)/vol.sum()
        center2 = np.sum(coords[sub2]*vol2)/vol2.sum()
        center_dev[i] = np.abs(center[i]-center2)
        mass = ca[sub]*vol
        ca_avg[i] = mass.mean()/vol.mean()
        ca_sd[i] = np.sqrt(1/npSv1*np.sum((ca_avg[i]-ca[sub])**2))
    interval2 = np.where((ca_avg  < (1-1e-5)) & (ca_avg  > 1e-5))
    inter2 = len(np.squeeze(interval2))
    center = center[interval2]
    center_dev = center_dev[interval2]
    ca_avg = ca_avg[interval2]
    ca_sd = ca_sd[interval2]
    center_dev[center_dev<1e-8] = 1e-8
    return center, center_dev, ca_avg, ca_sd

def effective_pore_volume(target, throat_volume='throat.volume', pore_volume='pore.volume'):
    pn = target.project.network
    Pvol = np.copy(target[pore_volume])
    Tvol = target[throat_volume]
    Vp = Pvol.sum() + Tvol.sum()
    np.add.at(Pvol, pn.conns[:, 0], target[throat_volume]/2)
    np.add.at(Pvol, pn.conns[:, 1], target[throat_volume]/2)
    assert np.isclose(Pvol.sum(), Vp)  # Ensure total volume has been added to Pvol
    return Pvol

def trans_eq_A4(beta, pe):
    return beta/np.tan(beta) - beta**2/pe + pe/4

def trans_eq_A4_prime(beta, pe):
    return 1/np.tan(beta)-beta/np.cos(beta)**2/np.tan(beta)**2-2*beta/pe

def roots_eq_A4(pe, n):
    beta = []
    for j in range(n):
        #1
        if j == 0:
            a = j*np.pi+1e-7
            b = (j+1)*np.pi
        else:
            a = j*np.pi
            b = a+1e-7

        fa = trans_eq_A4(a, pe)
        fb = trans_eq_A4(b, pe)

        #2
        x = (a+b)/2
        fx = trans_eq_A4(x, pe)
        dx = 100
        i = 0
        while (np.abs(dx) > 1e-12):
        #3
        #3.1
            fxl = trans_eq_A4_prime(x, pe)
            #3.2
            if fxl != 0:
                dx = - fx/fxl
                xnew = x+dx
                NR = 1
            else:
                dx = (b-a)/2
                NR = 0
            #3.3
            if NR == 0:
                #3.3.1
                if fx*fa < 0:
                    a = x
                    fa = fx
                else:
                    b = x
                    fb = fx
                #3.3.2
                x = (a+b)/2
                fx = trans_eq_A4(x, pe)
            #3.4
            else:
                #3.4.1
                x = xnew
                fx = trans_eq_A4(x, pe)
                if fx*fa < 0:
                    a = x
                    fa = fx
                #3.4.2
                else:
                    b = x
                    fb = fx
            i += 1
        beta.append(x)
        #print('i =', i, 'j = ', j, 'x = ', x, 'dx = ', np.abs(dx))
    return np.array(beta)

def c_an_A4_(xi, ti, D, c0, v, L, tol):
    pe = max(1e-5, v*L/D)
    #print('pe A4 = ', pe)
    if pe > 5.0:
        ci = c_an_A4_aprox(xi, ti, D, c0, v, L)
    else:
        ci = c_an_A4_serie(xi, ti, D, c0, v, L, tol, pe)
    return ci

def c_an_A4_serie(xi, ti, D, c0, v, L, tol, pe):
    ci = np.zeros(len(xi))
    nt = 20
    res = 100
#    bi = eigenvalues_A4(pe, nt)
    while res > tol and nt < 5000:
        bi = roots_eq_A4(pe, nt)
        res = 0
        for i in range(len(xi)):
            x = xi[i]
            t = ti[i]
            serie = 0
            mmax = len(bi)
            for j in range(mmax):
                b = bi[j]
                serie_old = serie
                s1 = 2*v*L/D*b*(b*np.cos(b*x/L)+v*L/2/D*np.sin(b*x/L))
                s2 = np.exp(v*x/2/D-v**2*t/4/D-b**2*D*t/L**2)
                s3 = (b**2+(v*L/2/D)**2+v*L/D)*(b**2+(v*L/2/D)**2)
                serie +=(s1*s2/s3)
            res = max(res, np.abs(serie-serie_old))
            ci[i] = c0*(1.0-serie)
        nt += 100
        if bi[-1] < 10 or bi[0] < 0:
            print('ERROR b[0], b[-1] ', bi[0], bi[-1])
    return ci

def c_an_A4_aprox_(x, t, D, c0, v, L):
    c1 = 0.5*erfc((x-v*t)/2.0/np.sqrt(D*t))
    c2 = np.sqrt(v**2*t/np.pi/D)*np.exp(-(x-v*t)**2/4.0/D/t)
    cexp = np.exp(v*x/D)
    c3 = -0.5*(1.0+v*x/D+v**2*t/D)*cexp*erfc((x+v*t)/2.0/np.sqrt(D*t))
    c4 = np.sqrt(4.0*v**2*t/np.pi/D)*(1.0+v/4.0/D*(2*L-x+v*t))
    c5 = np.exp(v*L/D-1/4.0/D/t*(2*L-x+v*t)**2)
    cexpL = np.exp(v*L/D)
    c6a = -v/D*(2*L-x+3.0*v*t/2+v/4.0/D*(2*L-x+v*t)**2)
    c6 = c6a*cexpL*erfc((2.0*L-x+v*t)/2.0/np.sqrt(D*t))
    c = c0*(c1+c2+c3+c4*c5+c6)
    return c

def c_an_A4_aprox(x, t, D, c0, v, L):
    #print('D ', D)
    c1 = 0.5*erfc((x-v*t)/2.0/np.sqrt(D*t))
    c2 = np.sqrt(v**2*t/np.pi/D)*np.exp(-(x-v*t)**2/4.0/D/t)
    c3 = -0.5*(1.0+v*x/D+v**2*t/D)
    y = v*x/D
    for i in range(len(c3)):
        z = (x[i]+v*t[i])/(2.0*np.sqrt(D*t[i]))
        if y[i] > 700:
            prod = 1.0
            sume = 1
            a = 10000
            j = 1
            while (np.abs(a)>1e-16):
                prod *= 2.0*j-1.0
                a = (-1)**j*prod/(2*z**2)**j
                sume += a
                j += 1 
            w =np.exp(y[i]-z**2)/np.sqrt(np.pi)/z*sume
        else:
            w = np.exp(y[i])*erfc(z)
        c3[i] *= w
    c4 = np.sqrt(4.0*v**2*t/np.pi/D)*(1.0+v/4.0/D*(2*L-x+v*t))
    c5 = np.exp(v*L/D-1/4.0/D/t*(2*L-x+v*t)**2)
    c6 = -v/D*(2*L-x+3.0*v*t/2+v/4.0/D*(2*L-x+v*t)**2)
    y = v*L/D
    if y > 700:
        for i in range(len(c6)):
            z = (2*L-x[i]+v*t[i])/(2*np.sqrt(D*t[i]))
            prod = 1.0
            sume = 1.0
            a = 1e1000
            j = 1
            while (np.abs(a)>1e-16):
                prod *= 2.0*j-1.0
                a = (-1)**j*prod/(2*z**2)**j
                sume += a
                j += 1 
            w=np.exp(y-z**2)/np.sqrt(np.pi)/z*sume
            c6[i] *= w
    else:
        c6 *= np.exp(y)*erfc((2.0*L-x+v*t)/2.0/np.sqrt(D*t))
    c = c0*(c1+c2+c3+c4*c5+c6)
    return c

def calc_Lstar(net):
    cn = net['throat.conns']
    C1 = net['pore.coords'][net['throat.conns'][:, 0]]
    C2 = net['pore.coords'][net['throat.conns'][:, 1]]
    return np.linalg.norm(C1 - C2, axis=1).mean()
