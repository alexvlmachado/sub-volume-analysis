import openpnm as op
import numpy as np

class networkProperties:
    # class constructor
    def __init__(self, pn):
        xLeft = pn.coords[:,0][pn.pores('left')].mean()
        xRight = pn.coords[:,0][pn.pores('right')].mean()
        self.sp = xLeft-pn.coords[:,0][pn.pores('inlet')].mean()
        self.Lx = xRight-xLeft+self.sp
        self.Ly = pn.coords[:,1].max()-pn.coords[:,1].min()+self.sp
        self.Lz = pn.coords[:,2].max()-pn.coords[:,2].min()+self.sp
        self.pn = pn

    def get_porosity(self):
        vol = self.Lx*self.Ly*self.Lz
        proj = self.pn.project
        geo = proj.geometries('geo_01')
        Vp = geo['pore.effective_volume'][self.pn.pores('internal')].sum()
        porosity=Vp/vol
        print('\nPorosity =', porosity)
        return porosity

    def get_permeability(self):
        pn = self.pn
        proj = pn.project
        #sf = proj.physics('alg_01')
        sf = proj.algorithms('alg_01')
#        sf.reset(bcs=True)
#        sf.set_value_BC(pores=pn.pores('inlet'), values=1)
#        sf.set_value_BC(pores=pn.pores('outlet'), values=0.0)
#        sf.run()
        Lsample = self.Lx-self.sp
        area = self.Ly*self.Lz
        perm = np.squeeze(sf.calc_effective_permeability(inlets=pn.pores('inlet'),
                      outlets=pn.pores('outlet'), domain_length= Lsample,
                      domain_area=area))
        print('\nPermeability =', perm)
        return perm

    def get_Lstar(self):
        proj = self.pn.project
        geo = proj.geometries('geo_01')
        Lstar=np.mean(geo['throat.conduit_lengths.pore1']+
                      geo['throat.conduit_lengths.throat']+
                      geo['throat.conduit_lengths.pore2'])
        print('\nMean conduit length  = ', Lstar)
        return Lstar

    def get_area(self):
        return self.Ly*self.Lz

