import openpnm as op
import numpy as np
import matplotlib.pyplot as plt
from sampling import *
import time
import os
from datetime import datetime
from simulationParameters import *
import sys
from networkProperties import networkProperties

netName = netName.split('.')[0]

ws = op.Workspace()
ws.settings["loglevel"] = 50

timeZero = time.time()
proj = op.io.PNM.load_project(netName)
print('Elapsed time reading ',netName ,'= ', (time.time()-timeZero)/60, ' min')

pn = proj.network
geo = proj.geometries('geo_01')
water = proj.phases('phase_01')
phys = proj.physics('phys_01')
sf = proj.physics('alg_01')

inlet=pn.pores('inlet')
outlet=pn.pores('outlet')
internal=pn.pores('internal')
inlet_t = pn.find_neighbor_throats(pores=inlet)
outlet_t = pn.find_neighbor_throats(pores=outlet)
inlet_nb = pn.find_neighbor_pores(pores=inlet)

print('number of pores =', pn.Np)

geo.add_model(propname='pore.effective_volume', model=effective_pore_volume)

prop = networkProperties(pn)
porosity = prop.get_porosity()
permeability = prop.get_permeability()
Lstar = prop.get_Lstar()
area = prop.get_area()
Lcol = prop.Lx-prop.sp

dif_model = op.models.physics.diffusive_conductance.ordinary_diffusion
ad_dif_model = op.models.physics.ad_dif_conductance.ad_dif

phys.add_model(propname='throat.diffusive_conductance', model=dif_model)
phys['throat.diffusive_conductance'][inlet_t] = 1e-50

Vp = geo['pore.effective_volume'][internal].sum()
Dm = water['pore.diffusivity'].mean()

mu = water['pore.viscosity'].mean()

deltaP = Vp*peclet*Dm*mu/Lstar/area/permeability
print('deltaP form simulation = ', deltaP)

sf.reset(bcs=True)
sf.set_value_BC(pores=inlet, values=deltaP)
sf.set_value_BC(pores=outlet, values=0.0)
sf.run()
water.update(sf.results())

flowRate = np.squeeze(sf.rate(pores=inlet))
vel = flowRate*Lcol/Vp
print('intrinsic velocity inlet m/s', vel)

pe = vel*Lstar/Dm
print('\nPeclet number - molecular ', pe)

phys.add_model(propname='throat.ad_dif_conductance', model=ad_dif_model, s_scheme='powerlaw')

ad = op.algorithms.TransientAdvectionDiffusion(network=pn, phase=water)
ad.set_value_BC(pores=inlet, values=1.0)
ad.set_outflow_BC(pores=outlet)
ad.set_IC(0)
t_initial = 0

resTime = np.around(geo['pore.effective_volume'].sum()/flowRate)
timeStep = 0.1*np.around(np.min(np.abs(geo['pore.effective_volume']/sf.rate(pores=pn.Ps, mode='single'))), 5)
print('\ntimeStep', timeStep)
totalSteps = int(Lcol/vel/timeStep)
nout = int(Lcol/vel/timeStep/500)+1
print('number of outputs', nout)
t_final = totalSteps*timeStep+t_initial
t_output = nout*timeStep
print('t_initial =', t_initial)
print('t_final = ', t_final)
print('t_output = ', t_output)

ad.setup(t_initial=t_initial, t_final=t_final, t_step=timeStep, t_output=t_output,
         t_tolerance=1e-12, t_precision=4, t_scheme='implicit', t_backup=10000)

ad.settings['pore_volume'] = 'pore.effective_volume'
print(ad.settings)
timeZero = time.time()
ad.run()
print('Elapsed time during adv diff simulation (min) = ', (time.time()-timeZero)/60) 

timeZero = time.time()
pn.project.save_project(filename= netName+'_ad_sim')
print('Elapsed time writing pnm file  ', (time.time()-timeZero)/60, ' min')

