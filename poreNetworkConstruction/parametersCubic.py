import numpy as np

porosity = 0.4
permeability = 100.0e-12
shape = np.array([500, 20, 20])
sp = 5.0e-4
sampleName = 'C1'
