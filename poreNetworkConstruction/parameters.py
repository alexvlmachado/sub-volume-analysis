import numpy as np

porosity = 0.3
permeability = 1.0e-12
z_mean = 6
shape = np.array([400, 20, 20])
sp = 1.0e-4
sampleName = 'CZ1'
