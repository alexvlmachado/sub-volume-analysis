## Algorithm for creating statically built pore networks
  
### Cubic Networks
Change input parameters in the parametersCubic.py file

Run the code:
```
python cubicNetwork.py
```

### Networks with variable connectivity
Change input parameters in parameters.py file

Run the code:
```
python customNetwork.py
```
